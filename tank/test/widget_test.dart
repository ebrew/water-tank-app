import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:tank/src/screens/login.dart';

// Widget Test
void main() {
  Widget makeTestableWidget({Widget child}){
    return MaterialApp(
      home: child,
    );
  }
  testWidgets('', (WidgetTester tester) async {
    await tester.pumpWidget(makeTestableWidget(child: Login()));

    // Verify that two textFields are found.
    var textField = find.byType(TextField);
    expect(textField, findsWidgets);

    // Verify that one login button is found.
    var button = find.text('LOGIN');
    expect(button, findsOneWidget);

    // Verify that our counter starts at 0.
    expect(find.text('0'), findsOneWidget);
    expect(find.text('1'), findsNothing);

    // Tap the '+' icon and trigger a frame.
    await tester.tap(find.byIcon(Icons.add));
    await tester.pump();

    // Verify that our counter has incremented.
    expect(find.text('0'), findsNothing);
    expect(find.text('1'), findsOneWidget);
  });
}