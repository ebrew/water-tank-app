import 'package:flutter_test/flutter_test.dart';
import 'package:tank/src/validation_mixin.dart';

// Unit tests
void main() {

  // Email validation test
  test('empty email returns error string', (){
    var result = ValidationMixin().validateEmail('');
    expect(result, 'Pls enter a valid email address');
  });

  test("email without '@' returns error string", (){
    var result = ValidationMixin().validateEmail('ebrew');
    expect(result, 'Pls enter a valid email address');
  });

  test("email '@' returns null", (){
    var result = ValidationMixin().validateEmail('ebrew@gmail.com');
    expect(result, null);
  });

  // Password validation test
  test('empty password returns error string', (){
    var result = ValidationMixin().validatePassword('');
    expect(result, 'Password must be at least 4 characters');
  });

  test("password less than 4 characters returns error string", (){
    var result = ValidationMixin().validatePassword('ebr');
    expect(result, 'Password must be at least 4 characters');
  });

  test("password exceeding 4 characters returns returns null", (){
    var result = ValidationMixin().validatePassword('ebrew');
    expect(result, null);
  });

  // User phone number validation test
  test('contact +233XXXXXXXXX or 0XXXXXXXXX returns null', (){
    var result = ValidationMixin().validateContact('+233249039730');
    expect(result, null);
  });

  test("contacts not upto 10 or more than 13 digits or empty returns error string", (){
    var result = ValidationMixin().validateContact('024903973');
    expect(result, 'Pls enter a valid phone number');
  });

  // Station ID validation test
  test('ID not exactly 12 digits returns error string', (){
    var result = ValidationMixin().validateStationID('20421129');
    expect(result, 'Base station ID must be 12 digits');
  });

  test('ID being exactly 12 digits returns null', (){
    var result = ValidationMixin().validateStationID('204211291234');
    expect(result, null);
  });

  // Sensor and actuator ID validation test
  test('ID not exactly 5 digits returns error string', (){
    var result = ValidationMixin().validateSensorID('20421129');
    expect(result, 'ID must be 5 digits');
  });

  test('ID being exactly 5 digits returns null', (){
    var result = ValidationMixin().validateSensorID('20421');
    expect(result, null);
  });

}