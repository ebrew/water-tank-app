class ValidationMixin {

  String validateEmail(String value){
    if (!value.contains('@')){
      return ('Pls enter a valid email address');
    } 
    return null;
  }

  String validatePassword(String value){
    if (value.length < 4){
      return ('Password must be at least 4 characters');
    }
    return null;
  }

  String validateContact(String value){
    if (value.length < 10 || value.length > 13){
      return ('Pls enter a valid phone number');
    }
    return null;
  }

  String fieldCheck(String value){
    if (value.isEmpty){
      return ('Field can not be empty');
    }
    else if(value.length > 0 && value.length < 3){
      return ('Enter a valid detail');
    }
    return null;
  }

  String validateStationID(String value){
    if (value.length != 12){
      return ('Base station ID must be 12 digits');
    }
    return null;
  }

  String validateSensorID(String value){
    if (value.length != 5){
      return ('ID must be 5 digits');
    }
    return null;
  }

}