import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:tank/src/screens/set_up/addSensor.dart';
import 'dart:convert';
import 'package:tank/src/validation_mixin.dart';

class AddBaseStation extends StatefulWidget{
  final String phoneNumber;
  AddBaseStation({Key key, this.phoneNumber}) : super(key: key);

  @override
  _AddBaseStationState createState() => _AddBaseStationState();
}

class _AddBaseStationState extends State<AddBaseStation> with ValidationMixin{
  final formkey = GlobalKey<FormState>(); //represents the formstate object
  bool _isLoading = false;
  String baseStationID = '';
  String baseStationName = '';
  String prompt = '';
  bool alert = false;

  SharedPreferences sharedPreferences; // variable for login token operations

  // checks the state in a non-stop manner
  @override
  void initState() {
    super.initState();
   checkSignUpStatus();
  }

  // consistently checks if sign up is successful
  checkSignUpStatus() async{
    sharedPreferences = await SharedPreferences.getInstance();
    if(sharedPreferences.getString("access_token") == null){ 
      _showToast();  
    } 
  }

  void _showToast() {
    Fluttertoast.showToast(
      msg: 'Kindly complete your setup account',
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.TOP,
      //timeInSecForIos: 1,
      backgroundColor: Color(0xfff5f5f5),
      textColor: Colors.black
    );
  }
 
  
 addBaseStation(baseStationID,  baseStationName) async{
    Map data = {
      'bs_number': baseStationID, 
      'bs_name': baseStationName,
      'api_key': 'AqwYuidfhfkff.tYnsdbsbs5.9fdhjGhsruk34fjjHYKADVDMdshhhhd2'
    };
    var jsonData;
    http.Response response = await http.post("https://accidental-survey.000webhostapp.com/sanitation_project/apis/mobile/client/setup.php/${widget.phoneNumber}", body: data);
    if (response.statusCode < 200 || response.statusCode > 400 || json == null) {
      throw Exception("Could'nt connect, please ensure you have a stable network or further chrck ur PC IP adresss.");
    }
    else {
      //prepares state for login token
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      jsonData = json.decode(response.body);
      String server_message = jsonData['message'];
      if(server_message.contains('True')){
        // when post is successful
        setState(() {
         _isLoading = false;
         sharedPreferences.setString("access_token", jsonData['access_token']);
         // passing data while routing to a new page
         var route = MaterialPageRoute(
           builder: (BuildContext context) => AddSensor(phoneNumber: widget.phoneNumber,)
         );
         Navigator.of(context).push(route);
         //Navigator.pushNamed(context, "/Home"); 
        });
      } else {
        print(jsonData);
        setState(() {
          alert = true;
          prompt = server_message;
          _isLoading = false;  
        });
      }  
    }
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Add Base Station'),
        backgroundColor: Colors.red,
        elevation: 0,
        iconTheme:  IconThemeData(color: Colors.white),
      ),
      backgroundColor: Color(0xfff5f5f5),
      body: _body(),
    );
  }

  _body(){
    Size size = MediaQuery.of(context).size;
    return Center(
        child: _isLoading ? Center(child: CircularProgressIndicator()) : Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Form( 
              key: formkey,               
              child: Container(
                margin: EdgeInsets.only(left:15, right: 15),
                padding: EdgeInsets.all(10.0),
                child: Column(
                  children: <Widget>[
                    Card(
                      child: TextFormField( 
                        autofocus: false,
                        keyboardType: TextInputType.phone,
                        decoration: InputDecoration(
                          prefixIcon: Icon(Icons.account_box),
                          labelText: 'Base Station ID',
                          //hintText: '12'
                        ),
                        validator: validateStationID,
                        onSaved: (String value){
                          baseStationID = value;
                        },
                      ),
                    ),
                   SizedBox(height:15),
                   Card(
                    child: TextFormField(
                      autofocus: false,
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.person_outline),
                        labelText: 'Base Station Name(Optional)',
                      ),
                      onSaved: (String value){
                        (value == null) ? baseStationName = baseStationID: baseStationName = value;
                        // if(value == null){
                        //   baseStationName = baseStationID;
                        // } else {baseStationName = value;}
                      },
                    ),
                  ),
                ] 
              )
            )
          ),
          Padding(
            padding: EdgeInsets.only(top:10),
            child: MaterialButton(
              onPressed: (){
                if(formkey.currentState.validate()){
                  formkey.currentState.save();
                  Navigator.pushNamed(context, "/AddSensor");
                  // setState(() {
                  //   _isLoading = true;
                  // });
                  // login(email, password);
                  // formkey.currentState.reset();  
                  // alert ? _showToast() : prompt= '';                    
                }
              },
              child: Text('Proceed',
                style: TextStyle(
                  fontSize: 15,
                  fontFamily: 'fira',
                  fontWeight: FontWeight.bold,
                ),
              ),
              color: Colors.red,
              elevation: 0,
              minWidth: 350,
              height: 55,
              textColor: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5)
              ),
            ),
          ),
        ]
      ) 
    );
  }
}