import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:tank/src/screens/home.dart';
import 'dart:convert';
import 'package:tank/src/validation_mixin.dart';

class AddActuator extends StatefulWidget{
  final String phoneNumber;
  AddActuator({Key key, this.phoneNumber}) : super(key: key);

  @override
  _AddActuatorState createState() => _AddActuatorState();
}

class _AddActuatorState extends State<AddActuator> with ValidationMixin{
  final formkey = GlobalKey<FormState>(); //represents the formstate object
  bool _isLoading = false;
  String actuatorID1 = '';
  var _tanklist = ['Please choose your sensor installation', 'Water tank', 'Sceptic tank'];
  var actuatorInstall1 = 'Please choose your sensor installation'; //where to install sensor one
  String actuatorID2 = '';
  String actuatorInstall2 = 'Please choose your sensor installation'; //where to install sensor two
  String prompt = '';
  bool alert = false;

 addActuator(sensorID1,  sensorID2) async{
    Map data = {'id': sensorID1, 'name': sensorID2};
    var jsonData;
    http.Response response = await http.patch("http://10.10.3.106:5000/login/${widget.phoneNumber}", body: data);
    if (response.statusCode < 200 || response.statusCode > 400 || json == null) {
        throw Exception("Could'nt connect, please ensure you have a stable network or further chrck ur PC IP adresss.");
    }
    else {
      jsonData = json.decode(response.body);
      String server_message = jsonData['message'];
      if(server_message.contains('True')){
        // when login is successful
        setState(() {
         _isLoading = false;
         var route = MaterialPageRoute(
           builder: (BuildContext context) => Home()
         );
         Navigator.of(context).push(route);
        });
      } else {
        print(jsonData);
        setState(() {
          alert = true;
          prompt = server_message;
          _isLoading = false;  
        });
      }  
    }
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: Text('Add Actuator'),
        backgroundColor: Colors.red,
        elevation: 0,
        iconTheme:  IconThemeData(color: Colors.white),
        actions: <Widget>[
            FlatButton(
              child: Text(
                'Skip',
                style: TextStyle(
                  fontSize: 18,
                  color: Colors.white,
                ),
              ),
              splashColor: Colors.red,
              onPressed: () {
                Navigator.pushNamed(context, "/Home");
              },
            ),
        ]
      ),
      backgroundColor: Color(0xfff5f5f5),
      body: _isLoading ? Center(child: CircularProgressIndicator()) : ListView(
        children: <Widget>[
          SizedBox(height: 100),
          _body()
        ]
      ),
    );
  }

  _body(){
    return Center(
        child:Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Form( 
              key: formkey,               
              child: Container(
                margin: EdgeInsets.only(left:15, right: 15),
                padding: EdgeInsets.all(10.0),
                child: Column(
                  children: <Widget>[
                    Card(
                      child: Padding(
                        padding: EdgeInsets.only(left: 10),
                        child: TextFormField( 
                          autofocus: false,
                          keyboardType: TextInputType.phone,
                          decoration: InputDecoration(
                            //prefixIcon: Icon(Icons.account_box),
                            labelText: '1. Sensor ID',
                            hintText: 'The ID of the first sensor'
                          ),
                          validator: validateSensorID,
                          onSaved: (String value){
                            actuatorID1 = value;
                          },
                        ),
                      ),
                    ),
                   SizedBox(height:10),
                   DropdownButton<String>(
                      items: _tanklist.map((String dropDownStringItem){
                        return DropdownMenuItem<String>(
                          value: dropDownStringItem,
                          child: Text(dropDownStringItem), //wrapping the item with text
                        );
                      }).toList(), //convert to list
                      //hint: Text('Please choose ur watertank type'),
                      onChanged: (String newValueSelected){
                        setState(() {
                          this.actuatorInstall1 = newValueSelected;
                        });
                      },
                      value: actuatorInstall1,
                    ),
                    Card(
                      child: Padding(
                        padding: EdgeInsets.only(left: 10),
                        child: TextFormField( 
                          autofocus: false,
                          keyboardType: TextInputType.phone,
                          decoration: InputDecoration(
                            //prefixIcon: Icon(Icons.account_box),
                            labelText: '2. Sensor ID',
                            hintText: 'The ID of the second actuator'
                          ),
                          validator: validateSensorID,
                          onSaved: (String value){
                            actuatorID2 = value;
                          },
                        ),
                      ),
                    ),
                   SizedBox(height:10),
                   DropdownButton<String>(
                      items: _tanklist.map((String dropDownStringItem){
                        return DropdownMenuItem<String>(
                          value: dropDownStringItem,
                          child: Text(dropDownStringItem), //wrapping the item with text
                        );
                      }).toList(), //convert to list
                      //hint: Text('Please choose ur watertank type'),
                      onChanged: (String newValueSelected){
                        setState(() {
                          this.actuatorInstall2 = newValueSelected;
                        });
                      },
                      value: actuatorInstall2,
                    ),
                ] 
              )
            )
          ),
          Padding(
            padding: EdgeInsets.only(top: 5),
            child: MaterialButton(
              onPressed: (){
                if(formkey.currentState.validate()){
                  formkey.currentState.save();
                  print('A1: $actuatorInstall1 and A2: $actuatorInstall2');
                  Navigator.pushNamed(context, "/Home");
                  // setState(() {
                  //   _isLoading = true;
                  // });
                  // login(email, password);
                  // formkey.currentState.reset();  
                  // alert ? _showToast() : prompt= '';                    
                }
              },
              child: Text('Done',
                style: TextStyle(
                  fontSize: 15,
                  fontFamily: 'fira',
                  fontWeight: FontWeight.bold,
                ),
              ),
              color: Colors.red,
              elevation: 0,
              minWidth: 350,
              height: 55,
              textColor: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5)
              ),
            ),
          ),
        ]
      ) 
    );
  }
}