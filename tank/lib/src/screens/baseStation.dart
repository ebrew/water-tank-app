import 'dart:convert';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'home.dart';
import './set_up/addBaseStation.dart';

// Data mappings
class GetData{
  final String baseStationID;
  final String baseStationName;
  final List<Details> details;

  GetData({this.baseStationID, this.baseStationName, this.details});

  factory GetData.fromJson(Map<String, dynamic> json){
    return GetData(baseStationID: json['baseStationID'],
    baseStationName: json['baseStationName'],
    details: (json['details'] as List).map((details) => Details.fromJson(details)).toList());
    
  }
}

class Details{
  final String sensorID1;
  final String sensorInstall1;
  final String sensorID2;
  final String sensorInstall2;
  final String actuatorID1;
  final String actuatorInstall1;
  final String actuatorID2;
  final String actuatorInstall2;

  Details(
    { 
      this.sensorID1, 
      this.sensorInstall1,
      this.sensorID2,
      this.sensorInstall2,
      this.actuatorID1, 
      this.actuatorInstall1,
      this.actuatorID2,
      this.actuatorInstall2
    }
  );

  factory Details.fromJson(Map<String, dynamic> json){
    return Details(
      sensorID1: json['sensorID1'],
      sensorInstall1: json['sensorInstall1'],
      sensorID2: json['sensorID2'],
      sensorInstall2: json['sensorInstall2'],
      actuatorID1: json['actuatorID1'],
      actuatorInstall1: json['actuatorInstall1'],
      actuatorID2: json['actuatorID2'],
      actuatorInstall2: json['actuatorInstall2'],
    );
  }
}

Future<List<Details>> fetchBaseStations() async{
  final response = await http.get("http://192.168.4.1/?Cone=C1");

  if(response.statusCode == 200){
    List details = json.decode(response.body) ['details'];
    return details.map((details) => Details.fromJson(details)).toList();
  } else {
    throw Exception('Could not fetch base station details');
  }
}

class BaseStation extends StatefulWidget{
  final String email;
  BaseStation({Key key, this.email}): super(key : key);

  @override
  _BaseStationState createState() => _BaseStationState();
}

class _BaseStationState extends State<BaseStation>{

  var list_details;
  var refreshKey = GlobalKey<RefreshIndicatorState>();
  SharedPreferences sharedPreferences;

  @override
  void initState(){
    super.initState();
    checkLoginStatus();
    refreshListDetails();
  }

  // consistently checks if login has been successful and takes respective action
  checkLoginStatus() async{
    sharedPreferences = await SharedPreferences.getInstance();
    if(sharedPreferences.getString("access_token") == null){ 
      Navigator.of(context).pushNamed("/Login");  
    } 
  }

  Future<Null>refreshListDetails(){
    refreshKey.currentState?.show (atTop : false);
    setState((){
      list_details = fetchBaseStations();
    });

    return null;
  }
  
  @override
  Widget build(BuildContext context){
    return Scaffold(
     appBar: AppBar(
        centerTitle: true,
        title: Text('Base Station Details'),
        backgroundColor: Colors.red,
        elevation: 0,
        iconTheme:  IconThemeData(color: Colors.white),
        actions: <Widget>[
            FlatButton(
              child: Icon(
                  Icons.add,
                  size: 18,
                  color: Colors.white
              ),
              splashColor: Colors.red,
              onPressed: () {
                var route = MaterialPageRoute(
                  builder: (BuildContext context) => AddBaseStation(phoneNumber: widget.email)
                );
                Navigator.of(context).push(route);
              },
            ),
        ]
     ),
     drawer: MyDrawer(),
     body: Center(
      child: RefreshIndicator(
        key: refreshKey, 
        onRefresh: refreshListDetails,
        child: FutureBuilder<List<Details>>(
          future: list_details,
          builder: (context, snapshot){
          if(snapshot.hasError){
            return Text('Jeph API Error: ${snapshot.error}');
          }
          else if(snapshot.hasData){
            List<Details> details = snapshot.data;
            return ListView(
              children: details.map((detail) => GestureDetector(
                onTap: (){},
                child: Card(
                  elevation: 0,
                  color: Colors.white,
                  margin: const EdgeInsets.symmetric(vertical: 0.0, horizontal: 15),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: const EdgeInsets.symmetric(horizontal:2),
                        width: 100,
                        height: 140,
                        child: Image.asset('assets/images/bkg.jpg'),
                      )
                    ],
                  ),
                )
              )).toList()
            );
          }
          return CircularProgressIndicator();
        }
      )
    ))); 
  }
}