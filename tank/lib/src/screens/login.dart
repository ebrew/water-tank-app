import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../validation_mixin.dart';
import './home.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> with ValidationMixin{
  final formkey = GlobalKey<FormState>(); //represents the formstate object
  bool _autoValidate = false;
  bool _obscureText = true;
  bool _isLoading = false;
  String email = '';
  var password;
  String prompt = '';
  bool alert = false;

  //linking to the backend
  login(email,  password) async{
    Map data = {'email': email, 'password': password};
    var jsonData;
    http.Response response = await http.post("http://10.10.3.105:5000/apis/mobile/client/login", body: data);
    if (response.statusCode < 200 || response.statusCode > 400 || json == null) {
        throw Exception("Could'nt connect, please ensure you have a stable network or further chrck ur PC IP adresss.");
    }
    else {
      //prepares state for login token
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      jsonData = json.decode(response.body);
      String server_message = jsonData['message'];
      if(server_message.contains('True')){
        // when login is successful
        setState(() {
         _isLoading = false;
         sharedPreferences.setString("access_token", jsonData['access_token']);
         var route = MaterialPageRoute(
           builder: (BuildContext context) => Home(email: email,)
         );
         Navigator.of(context).push(route); 
        });
      } else {
        print(jsonData);
        setState(() {
          alert = true;
          prompt = server_message;
          _isLoading = false;  
        });
      }  
    }
  }

  void _showToast() {
    Fluttertoast.showToast(
      msg: prompt,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.CENTER,
      //timeInSecForIos: 1,
      backgroundColor: Color(0xfff5f5f5),
      textColor: Colors.black
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    //To set the background image 
    return Scaffold(
      // body: _body(context),
      body: SafeArea( child:
       Stack(
        children: <Widget>[
          Center(
            child: Image.asset(
              'assets/images/bkg.jpg',
              width: size.width,
              height: size.height,
              fit: BoxFit.fill,
            ),
          ),
          // alert ? _showToast :
          _body(context)
        ]
      ),)
    ); 
  }

  Widget _body(BuildContext context){
    return Container(
      child: _isLoading ? Center(child: CircularProgressIndicator()) : ListView(
        shrinkWrap: true,
        padding: EdgeInsets.all(15.0),
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height:60),
              CircleAvatar(
                backgroundImage: AssetImage("assets/images/l.png"),
                radius: 60,
              ),
                SizedBox(height:10),
                Text(
                  'Watertank Sensor App',
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'fira',
                    fontSize: 20,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                _formUI()
                  ],
                )  
              ],
       ),
    );
  }

  Widget _formUI (){
    return Form( 
      key: formkey, 
      autovalidate: _autoValidate,               
      child: Container(
        padding: EdgeInsets.all(10.0),
        child: Column(
          children: <Widget>[
            Card(
              child: TextFormField( 
                autofocus: false,
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                  prefixIcon: Icon(Icons.contact_mail),
                  labelText: 'Email Address',
                  hintText: 'you@example.com'
                ),
                validator: validateEmail,
                onSaved: (String value){
                  email = value;
                },
              ),
            ),
            SizedBox(height:15),
            Card(
              child: TextFormField(
                autofocus: false,
                obscureText: _obscureText,
                decoration: InputDecoration(
                  suffixIcon: GestureDetector(
                    child: Icon(
                      _obscureText ? Icons.visibility : Icons.visibility_off,
                    ),
                    onTap: (){
                      setState(() {
                        _obscureText = !_obscureText;
                      });
                    }
                  ),
                  prefixIcon: Icon(Icons.lock),
                  labelText: 'Password',
                  hintText: 'Password'
                ), 
                validator: validatePassword,
                onSaved: (String value){
                  //  var value1 = utf8.encode(value);
                  //  password = sha256.convert(value1);
                  password = value;
                },
              ),
            ),
            Container(alignment: Alignment.center,
              child: Text(
                prompt,
                style: TextStyle(color: Colors.red, fontStyle: FontStyle.italic, fontSize: 17, fontWeight: FontWeight.w600)
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 5),
              child: MaterialButton(
                onPressed: (){
                  if(formkey.currentState.validate()){
                    formkey.currentState.save();
                    setState(() {
                      _isLoading = true;
                    });
                    //login(email, password);
                    var route = MaterialPageRoute(
                      builder: (BuildContext context) => Home(email: email,)
                    );
                    Navigator.of(context).push(route); 
                    formkey.currentState.reset();  
                    alert ? _showToast() : prompt= '';                    
                  }
                },
                child: Text('LOGIN',
                  style: TextStyle(
                    fontSize: 15,
                    fontFamily: 'fira',
                    fontWeight: FontWeight.bold,
                  ),
                ),
                color: Colors.red,
                elevation: 0,
                minWidth: 350,
                height: 55,
                textColor: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 10),
              child: Center(
                child: FlatButton(
                  child: Text('Forgot your password?',
                    style: TextStyle(
                      fontFamily: 'fira',
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      color: Colors.white
                    ),
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, "/retrieve");
                  },
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 0),
              child: Center(
                child: FlatButton(
                  onPressed: (){
                    Navigator.pushNamed(context, "/SignUp");
                  },
                child: RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text: "Don't have an account?",
                        style: TextStyle(
                          fontFamily: 'fira',
                          color: Colors.white,
                          fontSize: 15,
                          fontWeight: FontWeight.w500
                        )
                      ),
                      TextSpan(
                        text: "sign up",
                        style: TextStyle(
                          fontFamily: 'fira',
                          color: Colors.red,
                          fontSize: 15,
                          fontWeight: FontWeight.w500
                        )
                      )]
                    ),
                  ),
                ),
              )
            ),
          ],
        ),
      ),
    );
  }
}
