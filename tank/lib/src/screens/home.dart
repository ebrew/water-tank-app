import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:fluttertoast/fluttertoast.dart';
import './baseStation.dart';

class Home extends StatefulWidget{
  final String email; //value to be passed from login to homepage 
  Home({Key key, this.email}): super(key : key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  SharedPreferences sharedPreferences; // variable for login token operations

  // checks the state in a non-stop manner
  @override
  void initState() {
    super.initState();
    //checkLoginStatus();
  }

  // consistently checks if login access token is available and takes respective action
  checkLoginStatus() async{
    sharedPreferences = await SharedPreferences.getInstance();
    if(sharedPreferences.getString("access_token") == null){ 
      Navigator.of(context).pushNamed("/Login");  
    } else {
      print('Welcome ${widget.email}'); //testing
      _showToast();
    }
  }

  void _showToast() {
    Fluttertoast.showToast(
      msg: 'Successfully Logged in',
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      //timeInSecForIos: 1,
      backgroundColor: Colors.black12,
      textColor: Colors.black
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        centerTitle: false,
        title: Text('Home'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.exit_to_app),
            iconSize: 26, 
            onPressed: ()async{
              sharedPreferences = await SharedPreferences.getInstance();
              sharedPreferences?.clear(); 
              Navigator.of(context).pushNamed("/Login");
            },
          ),
        ],
      ),
      body: _homePage(context), 
      drawer: MyDrawer(), 
    );
  }

  //For Help & feedback popup
  createAlertDialog(BuildContext context){
    TextEditingController customController = TextEditingController();
    return showDialog(context: context, builder: (context){
      return AlertDialog(
        title: TextField(
          controller: customController,
          autofocus: true,
          decoration: InputDecoration(
              labelText: 'Help & Feedback', hintText: 'Tell us what you think',
              labelStyle: TextStyle(color: Colors.black, fontSize: 25, fontWeight: FontWeight.w600)),
        ),
        actions: <Widget>[
           FlatButton(
              child: const Text('HELP?'),
              onPressed: () {
                Navigator.pop(context);
              }
           ),
           SizedBox(width:30),
           FlatButton(
              child: const Text('CANCEL'),
              onPressed: () {
                Navigator.pop(context);
              }
            ),
           SizedBox(width:30),
           FlatButton(
              child: const Text('SUBMIT'),
              onPressed: () {
                Navigator.pop(context);
              }
           )
        ],
      );
    });
  } 

  // Quick Access buttons
  Widget _homePage(BuildContext context) {
    return ListView(
        children: <Widget> [
          _MyCarousel(),
          Card(
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.fromLTRB(0, 15,10,8 ),
                  child: Text(
                    'Quick Access',
                    style: TextStyle(color: Colors.black, fontSize:25)
                  ),
                ),
                Divider(),
                Row(
                 mainAxisAlignment: MainAxisAlignment.spaceAround,
                 children: <Widget>[
                   Column(
                     children: <Widget>[
                       SizedBox(
                         height: 72,
                         width: 72,
                         child: RaisedButton(
                          onPressed: () {
                            //Navigator.pushNamed(context, "/BaseStation");
                            var route = MaterialPageRoute(
                              builder: (BuildContext context) => BaseStation(email: widget.email,)
                            );
                            Navigator.of(context).push(route);
                          },
                          splashColor: Colors.red,
                          child: Icon(
                             Icons.directions_railway,
                             color: Colors.red,
                             size: 44.0,
                          ),
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(50.0))),
                          color: Colors.white,
                        ),
                       ),
                       SizedBox(height:5),
                       Text(
                         "Base Stations",
                          style: TextStyle(fontSize:12, fontWeight: FontWeight.w600),
                       )
                     ]
                   ),
                  // Column(
                  //    children: <Widget>[
                  //      SizedBox(
                  //        height: 72,
                  //        width: 72,
                  //        child: RaisedButton(
                  //         onPressed: () {},
                  //         splashColor: Colors.red,
                  //         child: Icon(
                  //            Icons.feedback,
                  //            color: Colors.red,
                  //            size: 44.0,
                  //         ),
                  //         shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(50.0))),
                  //         color: Colors.white,
                  //       ),
                  //      ),
                  //      SizedBox(height:5),
                  //      Text(
                  //        "Sewage Tank Level",
                  //         style: TextStyle(fontSize:12, fontWeight: FontWeight.w600),
                  //      )
                  //    ]
                  //  ),                 
                   Column(
                     children: <Widget>[
                       SizedBox(
                         height: 72,
                         width: 72,
                         child: RaisedButton(
                          onPressed: () {},
                          splashColor: Colors.red,
                          child: Icon(
                             Icons.move_to_inbox,
                             color: Colors.red,
                             size: 44.0,
                          ),
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(50.0))),
                          color: Colors.white,
                        ),
                       ),
                       SizedBox(height:5),
                       Text(
                         "Request Services",
                          style: TextStyle(fontSize:12, fontWeight: FontWeight.w600),
                       )
                     ]
                   ),
                ],
              ),
              SizedBox(height: 40),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Column(
                     children: <Widget>[
                       SizedBox(
                         height: 72,
                         width: 72,
                         child: RaisedButton(
                          onPressed: () {
                            Navigator.pushNamed(context, "/FetchContact");
                          },
                          splashColor: Colors.red,
                          child: Icon(
                             Icons.feedback,
                             color: Colors.red,
                             size: 44.0,
                          ),
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(50.0))),
                          color: Colors.white,
                        ),
                       ),
                       SizedBox(height:5),
                       Text(
                         "Important Notice",
                          style: TextStyle(fontSize:12, fontWeight: FontWeight.w600),
                       )
                     ]
                   ),
                  // Column(
                  //    children: <Widget>[
                  //      SizedBox(
                  //        height: 72,
                  //        width: 72,
                  //        child: RaisedButton(
                  //         onPressed: () {
                  //           Navigator.pushNamed(context, "/FetchContact");
                  //         },
                  //         splashColor: Colors.red,
                  //         child: Icon(
                  //            Icons.feedback,
                  //            color: Colors.red,
                  //            size: 44.0,
                  //         ),
                  //         shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(50.0))),
                  //         color: Colors.white,
                  //       ),
                  //      ),
                  //      SizedBox(height:5),
                  //      Text(
                  //        "Dialy Tips",
                  //         style: TextStyle(fontSize:12, fontWeight: FontWeight.w600),
                  //      )
                  //    ]
                  //  ),
                  // SizedBox(width: 50),
                   Column(
                     children: <Widget>[
                       SizedBox(
                         height: 72,
                         width: 72,
                         child: RaisedButton(
                          onPressed: (){
                            createAlertDialog(context);
                          },
                          splashColor: Colors.red,
                          child: Icon(
                             Icons.help,
                             color: Colors.red,
                             size: 44.0,
                          ),
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(50.0))),
                          color: Colors.white,
                        ),
                       ),
                       SizedBox(height:5),
                       Text(
                         "Help & Feedback",
                          style: TextStyle(fontSize:12, fontWeight: FontWeight.w600),
                       )
                     ]
                   ),
                  ],
                ),
                Text('')
              ]
            ),
          ),
        ]
      );
  }
}


class _MyCarousel extends StatelessWidget{
  final carousel = Carousel(
    boxFit: BoxFit.fill,
    animationCurve: Curves.fastOutSlowIn,
    autoplayDuration: Duration(milliseconds: 5000),
    images: [
      AssetImage('assets/images/1.jpg'),
      AssetImage('assets/images/2.jpg'),
      AssetImage('assets/images/3.jpg'),
      AssetImage('assets/images/l.png'),
    ],
  );

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    return Container(
      child: Container(
        padding: EdgeInsets.all(5),
        height: (screenHeight*3)/8,
        child: ClipRRect(
          // borderRadius: BorderRadius.circular(15),
          child: Stack(
            children: <Widget>[
              carousel,
              // Banner(),
            ]
          )
      )
    ));
  }
}

class Banner extends StatefulWidget{
  @override
  _BannerState createState() => _BannerState();
}

class _BannerState extends State<Banner> with SingleTickerProviderStateMixin{

  AnimationController controller;
  Animation<double> animation;

  initState() {
    super.initState();
    controller = AnimationController(
      duration: Duration(milliseconds: 5000),
      vsync: this,
    );
    animation = Tween(begin: 0.0, end: 18.0).animate(controller)
      ..addListener((){
      controller.forward();
    });
  }

  @override
  Widget build(BuildContext context){
      return Padding(
        padding: const EdgeInsets.only(left:10, top:10),
        child: Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: Colors.amber.withOpacity(0.5),
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15),
              bottomRight: Radius.circular(15)
            )
          ),
          child: Text(
            'Watertank App features',
            style: TextStyle(
              fontFamily: 'fira',
              fontSize: 18
            ),
          ),
        ),
    );
  }

  dispose() {
    controller.dispose();
    super.dispose();
  }
}

class MyDrawer extends StatelessWidget{
  SharedPreferences sharedPreferences;
  
  @override
  Widget build(BuildContext context){
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
        // UserAccountsDrawerHeader(
        //       accountName: Text("Dev Team", style: TextStyle(color: Colors.greenAccent[700]),),
        //       accountEmail: Text("devteam@gmail.com", style: TextStyle(color: Colors.greenAccent[700]),),
        //       currentAccountPicture: GestureDetector(
        //         // onTap: ()=> print('Current pic'),
        //         child: CircleAvatar(
        //           backgroundImage: AssetImage('assets/images/l.png'),
        //           // backgroundImage: NetworkImage(
        //             //     'https://miro.medium.com/max/1400/1*uC0kYhn8zRx8Cfd0v0cYQg.jpeg'),
        //         )
        //       ),
        //       decoration: BoxDecoration(
        //         color: Colors.teal,
        //         image: DecorationImage(
        //           image: AssetImage("assets/images/ll.png"),
        //           fit: BoxFit.fill,
        //         )
        //       ),
        //     ),
        DrawerHeader(
          decoration: BoxDecoration(
            gradient: LinearGradient(colors: <Color>[
               Colors.red,
               Colors.deepOrange,
               Colors.orangeAccent, 
            ])
          ),
          child: Container(
            child: Column(
              children: <Widget>[
                Material(
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  elevation: 10,
                  child: Padding(padding: EdgeInsets.all(10.5),
                  child: Image.asset("assets/images/ll.png", width: 80, height: 80),
                  ),
                ),
                SizedBox(height:6),
                Text('Welcome ${Home().email}',
                  // 'Watertank Sensor App',
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.white
                    // fontWeight: FontWeight.bold,
                  )
                )],))
              ),
              ListTile(
                // trailing: Icon(Icons.home, size: 32, color: Colors.black,),
                leading: Icon(Icons.home, size: 32, color: Colors.black54,),
                title: Text(
                  'Home',
                  // style: TextStyle(color: Colors.red),
                ),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.of(context).pushNamed("/Home");               
                },
              ),
              // Divider(),
              ListTile(
                leading: Icon(Icons.directions_railway, size: 32, color: Colors.black54,),
                title: Text('Base Stations'),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.of(context).pushNamed("/BaseStation");
                },
              ),
              // ListTile(
              //   leading: Icon(Icons.feedback, size: 32, color: Colors.black54,),
              //   title: Text(' Sewage Tank Level'),
              //   onTap: () {
              //     Navigator.of(context).pop();
              //     Navigator.of(context).pushNamed("/Location");
              //   },
              // ),
              ListTile(
                leading: Icon(Icons.move_to_inbox, size: 32, color: Colors.black54,),
                title: Text('Request Service'),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.of(context).pushNamed("/Location");
                },
              ),
              ListTile(
                leading: Icon(Icons.feedback, size: 32, color: Colors.black54,),
                title: Text('Important Notice'),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.of(context).pushNamed("/Location");
                },
              ),
              ListTile(
                leading: Icon(Icons.help, size: 32, color: Colors.black54,),
                title: Text('Help & Feedback'),
                onTap: () {
                  Navigator.of(context).pop();
                  // createAlertDialog(context);
                  _HomeState().createAlertDialog(context);
                  // Navigator.of(context).pushNamed("/Feedbackh");
                },
              ),
              Divider(),
              ListTile(
                leading: Icon(Icons.exit_to_app, size: 32, color: Colors.black54,),
                title: Text(' Log Out'),
                onTap: () async{
                  sharedPreferences = await SharedPreferences.getInstance();
                  sharedPreferences?.clear();
                  Navigator.of(context).pop();
                  Navigator.of(context).pushNamed("/Login");
                },
              ),
              ListTile(
                leading: Icon(Icons.system_update, size: 32, color: Colors.black54,),
                title: Text('About'),
                subtitle: Text(
                  'version 1.0.13.2',

                ),
                onTap: () {
                  // Navigator.of(context).pop();
                },
              ),
            ],
          ),
        );

  }
}