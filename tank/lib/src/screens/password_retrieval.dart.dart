import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../validation_mixin.dart';

class RetrievePassword extends StatefulWidget {
  @override
  _RetrievePasswordState createState() => _RetrievePasswordState();
}

class _RetrievePasswordState extends State<RetrievePassword> with ValidationMixin{
  final formkey = GlobalKey<FormState>(); //represents the formstate object
  bool _isLoading = false;
  String phoneNumber = '';
  String prompt = '';
  bool alert = false;

  submit(password) async{
    Map data = {'email': "'No email'",'password': phoneNumber};
    var jsonData;
    http.Response response = await http.post("http://10.10.3.104:5000/reset", body: data);
    if (response.statusCode < 200 || response.statusCode > 400 || json == null) {
        throw Exception("Could'nt connect, please ensure you have a stable network or further chrck ur PC IP adresss.");
    }
    else {
      jsonData = json.decode(response.body);
      String server_message = jsonData['message'];
      if(server_message.contains('True')){
        setState(() {
         _isLoading = false;
         //Navigator.pushNamed(context, "/Login"); 
        });
      } else {
        print(jsonData);
        setState(() {
          alert = true;
          prompt = server_message;
          _isLoading = false;  
        });
      }  
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Password Retrieval'),
        backgroundColor: Colors.red,
        elevation: 0,
        iconTheme:  IconThemeData(color: Colors.white),
      ),
      body: _body(context),
    ); 
  }

  Widget _body(BuildContext context){
    double screenHeight = MediaQuery.of(context).size.height;
    return Container(
      height: screenHeight,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Colors.orangeAccent.withOpacity(0.5), //opacity is for the shade
            Colors.deepOrange.withOpacity(0.8), 
          ]
        )
      ),
      child: _isLoading ? Center(child: CircularProgressIndicator()) : ListView(
        shrinkWrap: true,
        padding: EdgeInsets.all(15.0),
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height:100),
              CircleAvatar(
                backgroundImage: AssetImage("assets/images/l.png"),
                radius: 60,
              ),
              SizedBox(height:10),
              Text(
                'Watertank Sensor App',
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'fira',
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          ),
          _formUI()  
        ],
       ),
    );
  }

  Widget _formUI (){
    return Form( 
      key: formkey,                
      child: Container(
        padding: EdgeInsets.all(10.0),
        child: Column(
          children: <Widget>[
            Card(
              child: TextFormField( 
                keyboardType: TextInputType.phone,
                decoration: new InputDecoration(
                  prefixIcon: Icon(Icons.contact_phone),
                    labelText: 'Phone Number',
                    hintText: '0XXXXXXXXX or 233XXXXXXXXX'
                ),
                validator: validateContact,
                onSaved: (String value){
                  phoneNumber = value;
                },
              )
            ),
            SizedBox(height: 0.0),
            Container(alignment: Alignment.center,
              child: Text(
                prompt,
                style: TextStyle(color: Colors.red, fontSize: 17, fontWeight: FontWeight.w600)
              ),
            ),
            Card(child:    
              SizedBox(width:350, height: 57,
                child: RaisedButton(
                  onPressed: (){
                    if(formkey.currentState.validate()){
                      formkey.currentState.save();
                      setState(() {
                        _isLoading = true;
                      });
                      submit(phoneNumber);
                      formkey.currentState.reset();                         
                    } 
                  },
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5)
                  ),
                  padding: EdgeInsets.all(12),
                  color: Colors.red,
                  child: Text('Send Reset link', style: TextStyle(color: Colors.white, fontSize: 20)),
                ),)
            ),
          ],
        ),
      ),
    );
  }
}

