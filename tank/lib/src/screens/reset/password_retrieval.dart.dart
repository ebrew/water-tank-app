import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:tank/src/screens/reset/reset_password.dart';
import 'dart:convert';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:tank/src/validation_mixin.dart';

class RetrievePassword extends StatefulWidget {
  @override
  _RetrievePasswordState createState() => _RetrievePasswordState();
}

class _RetrievePasswordState extends State<RetrievePassword> with ValidationMixin{
  final formkey = GlobalKey<FormState>(); //represents the formstate object
  bool _isLoading = false;
  String email = '';
  String link;
  String _warning;
  String prompt='';
  bool alert = false;

  void _showToast() {
    Fluttertoast.showToast(
      msg: prompt,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.CENTER,
      //timeInSecForIos: 1,
      backgroundColor: Color(0xfff5f5f5),
      textColor: Colors.black
    );
  }

  submit(email) async{
    Map data = {'email': email};
    var jsonData;
    http.Response response = await http.post("http://192.168.43.219:5000/apis/mobile/client/password_recovery/reset", body: data);
    if (response.statusCode < 200 || response.statusCode > 400 || json == null) {
        throw Exception("Could'nt connect, please ensure you have a stable network or further chrck ur PC IP adresss.");
    }
    else {
      jsonData = json.decode(response.body);
      String server_message = jsonData['link'];
      if(server_message.contains('/')){
        setState(() {
          _warning = "A password reset link has been sent to $email";
          print(_warning);
          alert = true;
          link = server_message;
         _isLoading = false;
         var route = MaterialPageRoute(
           builder: (BuildContext context) => ResetPassword(email: email, link: link)
         );
         Navigator.of(context).push(route);
        });
      } else {
        print(jsonData);
        setState(() {
          prompt=server_message;
          alert = true;
          _isLoading = false;  
        });
      }  
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Password Retrieval'),
        backgroundColor: Colors.red,
        elevation: 0,
        iconTheme:  IconThemeData(color: Colors.white),
      ),
      backgroundColor: Color(0xfff5f5f5),
      body: _body(context),
    ); 
  }

  Widget _body(BuildContext context){
    double screenHeight = MediaQuery.of(context).size.height;
    return Container(
      height: screenHeight,
      child: _isLoading ? Center(child: CircularProgressIndicator()) : ListView(
        shrinkWrap: true,
        padding: EdgeInsets.all(15.0),
        children: <Widget>[
          SizedBox(height: 140),
          _formUI()  
        ],
       ),
    );
  }

  Widget _formUI (){
    return Form( 
      key: formkey,                
      child: Container(
        padding: EdgeInsets.all(10.0),
        child: Column(
          children: <Widget>[
            Container(alignment: Alignment.center,
              child: Text(
                prompt,
                style: TextStyle(color: Colors.red, fontStyle: FontStyle.italic, fontSize: 17, fontWeight: FontWeight.w600)
              ),
            ),
            Card(
              child: TextFormField( 
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                  prefixIcon: Icon(Icons.contact_mail),
                    labelText: 'Email Address',
                    hintText: 'you@example.com'
                ),
                validator: validateEmail,
                onSaved: (String value){
                  email = value;
                },
              )
            ),
            Card(child:    
              SizedBox(width:350, height: 57,
                child: RaisedButton(
                  onPressed: (){
                    if(formkey.currentState.validate()){
                      formkey.currentState.save();
                      setState(() {
                        _isLoading = true;
                      });
                      submit(email);
                      formkey.currentState.reset(); 
                      if(alert==true){_showToast();}                        
                    } 
                  },
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5)
                  ),
                  padding: EdgeInsets.all(12),
                  color: Colors.red,
                  child: Text('Send Reset link', style: TextStyle(color: Colors.white, fontSize: 20)),
                ),)
            ),
          ],
        ),
      ),
    );
  }
}

