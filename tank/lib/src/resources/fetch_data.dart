import 'dart:convert';
import 'dart:async';
import 'package:flutter/material.dart';

class Contact{
  final String name;
  final String email;

  Contact(this.name, this.email);
}

class FetchContact extends StatelessWidget{

  Future<List<Contact>> getContacts(BuildContext context) async{
    var jsonData = await DefaultAssetBundle.of(context) //http.get('endpoint')
      .loadString("assets/images/contacts.json");
    List<dynamic> rawData = jsonDecode(jsonData);
     // return rawData.map(f() => Contact.fromJSON(f)).toList();
    List<Contact> contacts = [];
    for( var c in rawData){
      Contact contact = Contact(c["name"], c["email"]);
      contacts.add(contact);
    }
    print(contacts.length);
    return contacts;

  }
  @override
  Widget build(BuildContext context){
    return Scaffold(
     appBar: AppBar(title: Text('Contacts'),),
     body:Container(
      child: FutureBuilder(
        future: getContacts(context),
        builder: (context, snapshot){
          if(snapshot.hasData){
            List<Contact> data = snapshot.data;

            return ListView.builder(
              itemCount: data.length,
              itemBuilder: (context, index){
                return ListTile(
                  title: Text(
                    data[index].name,
                    style: TextStyle(fontSize: 25),
                  ),
                  subtitle: Text(
                    data[index].email,
                    style: TextStyle(fontSize: 20),
                  ),
                );
              }
            );
          }
          else {
            return Center(
              child: CircularProgressIndicator()
            );
          }
        }
      ),
    ));
  }
}