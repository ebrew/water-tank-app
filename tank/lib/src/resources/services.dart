import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../resources/services.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/services.dart';


class LoginHandler {

  static Future<LoginResponse> login(String email, String password) async{
    Map data = {'email': email, 'password': password};
    var jsonData;
    http.Response response = await http.post("http://10.10.3.103:5000/login", body: data);
    if (response.statusCode < 200 || response.statusCode > 400 || json == null) {
      return LoginResponse(httpConnection: false);
    }
    else {
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      jsonData = json.decode(response.body);
      String server_message = jsonData['message'];
      if(server_message.contains('Logged in as')){
        // setState(() {
        //  _isLoading = false;
        //  sharedPreferences.setString("access_token", jsonData['access_token']); //Doesn't work
        //  Navigator.pushNamed(context, "/Home"); 
        // });
        return LoginResponse(loginState: 'success', jsonData: jsonData);
      } else {
        return LoginResponse(loginState: 'failed');
        // print(jsonData);
        // setState(() {
        //   // alert = AlertDialog(
        //   //   title: Text(server_message),
        //   //   titleTextStyle: TextStyle(fontSize:25),
        //   //   actions: <Widget>[
        //   //     RaisedButton(onPressed: (){Navigator.of(context).pop();}, child: Text('Re-Login'))
        //   //   ],
        //   // );
        //   prompt = server_message;
        //   _isLoading = false;
          
        // });
      }  
    }
  }
}

class LoginResponse{
  bool httpConnection;
  String loginState;
  var jsonData;

  LoginResponse({this.httpConnection, this.loginState, this.jsonData});
}