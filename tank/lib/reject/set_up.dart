// import 'package:flutter/material.dart';
// import '../validation_mixin.dart';
// import 'package:multiselect_formfield/multiselect_formfield.dart';
// import 'package:http/http.dart' as http;
// import 'dart:convert';

// class SetUp extends StatefulWidget{
//   @override
//   _SetUpState createState ()=> _SetUpState();

// }

// enum Q1 { yes, no }
// Q1 _answer;
// // Q1 _answer = Q1.yes; // Default answer

// class _SetUpState extends State<SetUp> with ValidationMixin{

//   final formkey = GlobalKey<FormState>(); 
//   String stationID = '';
//   bool _isLoading = false;
//   List sensors;
//   String sensorID;
//   List<String> tanks = ['50K Tank', '60K Tank', '70K Tank', '80K Tank', '90K Tank', '100K Tank', 'Others'];
//   List<String> selectedTanks = [];

//   @override
//   void initState() {
//     super.initState(); 
//     sensors = []; // prompt the user if no sensor is selected
//     sensorID = '';
//   }

//   @override
//   Widget build(BuildContext context){
//     return Scaffold(
//       appBar: AppBar(
//         centerTitle: true,
//         title: Text('Account set up'),
//         backgroundColor: Colors.red,
//         elevation: 0,
//         iconTheme:  IconThemeData(color: Colors.white),
//       ),
//       backgroundColor: Color(0xfff5f5f5),
//       body: Center(
//         child: _isLoading ? Center(child: CircularProgressIndicator()) : ListView(
//           shrinkWrap: true,
//           padding: EdgeInsets.all(15),
//           children: <Widget>[
//             Column(
//               mainAxisAlignment: MainAxisAlignment.center,
//               children: <Widget>[
//                 CircleAvatar(
//                   backgroundImage: AssetImage("assets/images/l.png"),
//                   radius: 50,
//                 ),
//                 SizedBox(height:8),
//                 Text(
//                   'Watertank Sensor App',
//                   style: TextStyle(
//                     fontSize: 20,
//                     fontFamily: 'fira',
//                     fontWeight: FontWeight.w200
//                   ),
//                 ),
//               ]
//             ),
//             Center(
//               child: Form(
//                 key: formkey,
//                 child: Column(
//                   crossAxisAlignment: CrossAxisAlignment.center,
//                   children: <Widget>[
//                     Card(
//                       child: Column(
//                         children: <Widget>[
//                           Padding(
//                             padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 0),
//                             child: TextFormField(
//                               keyboardType: TextInputType.phone, 
//                               decoration: new InputDecoration(
//                                 labelText: 'Base Station ID',
//                               ),
//                               validator: fieldCheck,
//                               onSaved: (String value){
//                                 stationID= value;
//                               },                     
//                             )
//                           ),
//                           SizedBox(height: 8),
//                           Padding(
//                             padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 0),
//                             child: MultiSelectFormField(
//                             autovalidate: false,
//                             titleText: 'Sensor ID',
//                             validator: (value) {
//                               if (value == null || value.length == 0) {
//                                 return 'Please select one or more sensors';
//                               }
//                             },
//                             dataSource: [
//                               {
//                                 "display": "Water level sensor",
//                                 "value": "111",
//                               },
//                               {
//                                 "display": "Sensor2",
//                                 "value": "222",
//                               },
//                               {
//                                 "display": "Sensor3",
//                                 "value": "333",
//                               },
//                             ],
//                             textField: 'display',
//                             valueField: 'value',
//                             okButtonLabel: 'Continue',
//                             cancelButtonLabel: 'Cancel',
//                             hintText: 'Please choose one or more',
//                             value: sensors,
//                             onSaved: (value) {
//                               if (value == null) return;
//                               setState(() {
//                                 sensors = value;
//                               });
//                             },
//                           ),
//                         ),
//                         SizedBox(height: 8),
//                         _radioButtons(),
//                        ]
//                       ),
//                     ),
//                     SizedBox(height: 8),
//                     Padding(
//                       padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
//                       child: RaisedButton(
//                           color: Colors.red,
//                           shape: RoundedRectangleBorder(
//                             borderRadius: BorderRadius.circular(30.0),
//                             side: BorderSide(color: Color(0xFF179CDF))
//                           ),
//                           child: Text(' Done ', style: TextStyle(fontSize:25.0, color: Colors.white )),
//                           onPressed: (){
//                             if(formkey.currentState.validate()){
//                               formkey.currentState.save();
//                               setState(() {
//                                 sensorID = sensors.toString();
//                                 print(sensorID);
//                                 //  _isLoading = true;
//                               });
//                               //sign_up(fname,mname, lname, phoneNumber, email, password, location);
//                               formkey.currentState.reset(); 
//                               Navigator.pushNamed(context, "/Login");                          
//                             }
//                           },
//                       )
//                     ),
//                   ],
//                 )
//               )
//             )
//           ]
//         ) 
//       )
//     );
//   } 
//   Widget _radioButtons(){
//     return Container(
//       child: Column(
//         children: <Widget>[
//           Text(
//             'Please do you have any water tank already installed?',
//             style: TextStyle(fontFamily: 'fira', fontSize: 15)
//           ),
//           RadioListTile<Q1>(
//             title: const Text('Yes'),
//             value: Q1.yes,
//             groupValue: _answer,
//             onChanged: (Q1 value) {
//               showDialog(
//                 context: context,
//                 builder: (context) {
//                   return _AlertYesSelected(
//                     tanks: tanks,
//                     selectedTanks: selectedTanks,
//                     onSelectedTanksListChanged: (tanks) {
//                       selectedTanks = tanks;
//                       print(selectedTanks);
//                     }
//                   );
//                 }
//               );
//               setState(() { _answer = value; }); 
//             },
//           ),
//           RadioListTile<Q1>(
//             title: const Text('No'),
//             value: Q1.no,
//             groupValue: _answer,
//             onChanged: (Q1 value) { 
//               _showAlertDialogNo(context);
//               setState(() { _answer = value; }); 
//             },
//           ),
//         ],
//       )
//     );
//   } 

//   _showAlertDialogNo(BuildContext context) {
//     Widget yes = FlatButton(
//       child: Text("Yes"),
//       onPressed:  () {
//         Navigator.pop(context);
//         showDialog(
//           context: context,
//           builder: (context) {
//             return _AlertYesSelected(
//               tanks: tanks,
//               selectedTanks: selectedTanks,
//               onSelectedTanksListChanged: (tanks) {
//                 selectedTanks = tanks;
//                 print(selectedTanks);
//               }
//             );
//           }
//         );
//       },
//     );
//     Widget no = FlatButton(
//       child: Text("No"),
//       onPressed:  () {
//         Navigator.pop(context);
//       },
//     );
//     AlertDialog alert = AlertDialog(
//       title: Text("Response"),
//       content: Text("Would you need water tank installation?"),
//       actions: [
//         yes,
//         no,
//       ],
//     );
//     showDialog(
//       context: context,
//       builder: (BuildContext context) {
//         return alert;
//       },
//     );
//   }
// }

// class _AlertYesSelected extends StatefulWidget {
//   _AlertYesSelected({
//     this.tanks,
//     this.selectedTanks,
//     this.onSelectedTanksListChanged,
//   });

//   final List<String> tanks;
//   final List<String> selectedTanks;
//   final ValueChanged<List<String>> onSelectedTanksListChanged;

//   @override
//   _AlertYesSelectedState createState() => _AlertYesSelectedState();
// }

// class _AlertYesSelectedState extends State<_AlertYesSelected> {
//   List<String> _tempSelectedTanks = [];

//   @override
//   void initState() {
//     _tempSelectedTanks = widget.selectedTanks; // keeps record of the tank selection
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Dialog(
//       child: Column(
//         children: <Widget>[
//           Row(
//             mainAxisAlignment: MainAxisAlignment.spaceAround,
//             children: <Widget>[
//               Text(
//                 'Choose',
//                 style: TextStyle(fontSize: 20.0, fontFamily: 'fira', fontWeight: FontWeight.w600),
//                 textAlign: TextAlign.center,
//               ),
//               RaisedButton(
//                 onPressed: () {
//                   Navigator.pop(context);
//                 },
//                 color: Colors.red,
//                 child: Text(
//                   'Continue',
//                   style: TextStyle(color: Colors.white,fontFamily: 'fira',fontSize: 18, fontWeight: FontWeight.w600),
//                 ),
//               ),
//             ],
//           ),
//           Expanded(
//             child: ListView.builder(
//                 itemCount: widget.tanks.length,
//                 itemBuilder: (BuildContext context, int index) {
//                   final tankName = widget.tanks[index];
//                   return Container(
//                     child: CheckboxListTile(
//                         title: Text(tankName),
//                         value: _tempSelectedTanks.contains(tankName),
//                         onChanged: (bool value) {
//                           if (value) {
//                             if (!_tempSelectedTanks.contains(tankName)) {
//                               setState(() {
//                                 _tempSelectedTanks.add(tankName);
//                               });
//                             }
//                           } else {
//                             if (_tempSelectedTanks.contains(tankName)) {
//                               setState(() {
//                                 _tempSelectedTanks.removeWhere(
//                                     (String tank) => tank == tankName);
//                               });
//                             }
//                           }
//                           widget
//                               .onSelectedTanksListChanged(_tempSelectedTanks);
//                         }),
//                   );
//                 }),
//           ),
//         ],
//       ),
//     );
//   }
// }