import 'package:flutter/material.dart';
import './src/screens/login.dart';
import './src/resources/fetch_data.dart';
import './src/screens/home.dart';
import './src/screens/signup.dart';
import './src/screens/reset/password_retrieval.dart.dart';
import './src/screens/reset/reset_password.dart';
import './src/screens/set_up/addBaseStation.dart';
import './src/screens/set_up/addSensor.dart';
import './src/screens/set_up/addActuator.dart';
import './src/screens/baseStation.dart';


//Entrypoint
void main() => runApp(App());

class App extends StatelessWidget {
  // This widget is the root of the application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Login(),  
      debugShowCheckedModeBanner: false,

      //Routings
      routes: <String, WidgetBuilder> {
        "/Login":(context) =>Login(),
        "/Home" : (context) => Home(),
        "/SignUp" : (context) => SignUp(),
        "/AddBaseStation":(context) =>AddBaseStation(),
        "/retrieve" : (context) => RetrievePassword(),
        "/reset" : (context) => ResetPassword(),
        "/FetchContact" : (context) => FetchContact(),
        "/AddSensor" : (context) => AddSensor(),
        "/AddActuator" : (context) => AddActuator(),
        "/BaseStation" : (context) => BaseStation(),
      },
    );
  }
}


