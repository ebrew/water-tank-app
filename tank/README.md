# tank
Water Tank Sensor App.
This project titled water tank sensor app is a mobile application integrated with an IOT device

# How to run the app
To run this app, you need to install all the flutter required packages by:
1. Open the project in a VS Code environment.
2. Right click on the "pubspec.yaml" file and then select "Get Packages" to install all the 
   required packages for the app.
3. After the installation, run "flutter run" in the VS Code terminal.
Please it's an android app so make sure an android phone is connected


# How to run the unit test of the app
run "flutter test .\test\unit_test.dart" in the VS Code terminal.
To cover about 80% coverage test of the app, unit test alone would not be enough so i have added few widget test to support the unit test.
I would commit again which would include the more of the widget test.

